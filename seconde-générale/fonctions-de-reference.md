# Math

## Subdeck: Seconde générale

### Subdeck: Fonctions de référence

#### Subdeck: Fonction carré

##### Comment s'appelle la fonction définie par $x\mapsto x^2$ ?

Fonction carré

##### Quelle est l'expression de la fonction carré ?

$x\mapsto x^2$

##### Quel est le domaine de définition de la fonction carré ?

$\mathbb{R}$

##### Comment s'appelle la fonction représentée ici ?

![représentation](fonctions-de-reference/carre.png)

---

Fonction carré.

##### Quelle est la représentation graphique de la fonction carré ?

![représentation](fonctions-de-reference/carre.png)

##### Quel est le tableau de variations de la fonction carré ?

![variations](fonctions-de-reference/carre-variations.png)

##### Quel est le tableau de signes de la fonction carré ?

![signes](fonctions-de-reference/carre-signes.png)

##### À quelles conditions sur $a$ l'équation $x^2=a$ a-t-elle des solutions ?

Si $a\geq0$.

##### Quelles sont les solutions de $x^2=a$ (avec $a<0$) ?

Pas de solutions.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/carre-egale-negatif.png)

##### Quelles sont les solutions de $x^2=0$ ?

Une seule solution : $x=0$.

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/carre-signes.png)

##### Quelles sont les solutions de $x^2=a$ (avec $a>0$) ?

Deux solutions : $x=\sqrt{a}$ et $x=-\sqrt{a}$.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/carre-egale-positif.png)

##### Quelles sont les solutions de $x^2\leq a$ (avec $a<0$) ?

Pas de solutions.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/carre-egale-negatif.png)

##### Quelles sont les solutions de $x^2\leq 0$ ?

Une seule solution : $x=0$.

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/carre-signes.png)

##### Quelles sont les solutions de $x^2\leq a$ (avec $a>0$) ?

$x\in\left[-\sqrt{a};\sqrt{a}\right]$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/carre-inferieur-positif.png)

##### Quelles sont les solutions de $x^2\geq a$ (avec $a<0$) ?

Tous les nombres réels sont solution : $x\in\mathbb{R}$.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/carre-egale-negatif.png)

##### Quelles sont les solutions de $x^2\geq 0$ ?

Tous les nombres réels sont solution : $x\in\mathbb{R}$.

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/carre-signes.png)

##### Quelles sont les solutions de $x^2\geq a$ (avec $a>0$) ?

$x\in\left]-\infty;-\sqrt{a}\right]\cup\left[\sqrt{a};+\infty\right[$.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/carre-superieur-positif.png)

#### Subdeck: Fonction inverse

##### Comment s'appelle la fonction définie par $x\mapsto \frac{1}{x}$ ?

Fonction inverse

##### Quelle est l'expression de la fonction inverse ?

$x\mapsto \frac{1}{x}$

##### Quel est le domaine de définition de la fonction inverse ?

Tous les nombres réels sauf zéro : $\mathbb{R}^*$

##### Comment s'appelle la fonction représentée ici ?

![représentation](fonctions-de-reference/inverse.png)

---

Fonction inverse.

##### Quelle est la représentation graphique de la fonction inverse ?

![représentation](fonctions-de-reference/inverse.png)

##### Quel est le tableau de variations de la fonction inverse ?

![variations](fonctions-de-reference/inverse-variations.png)

##### Quel est le tableau de signes de la fonction inverse ?

![signes](fonctions-de-reference/inverse-signes.png)

##### À quelles conditions sur $a$ l'équation $\frac{1}{x}=a$ a-t-elle des solutions ?

Si $a\neq0$.

##### Quelles sont les solutions de $\frac{1}{x}=a$ (avec $a\neq0$) ?

Une seule solution : $x=\frac{1}{a}$.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/inverse-egale-non-nul.png)

##### Quelles sont les solutions de $\frac{1}{x}=0$ ?

Pas de solutions.

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/inverse-signes.png)

##### Quelles sont les solutions de $\frac{1}{x}\leq a$ (avec $a<0$) ?

$x\in\left[\frac{1}{a};0\right[$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/inverse-inferieur-negatif.png)

##### Quelles sont les solutions de $\frac{1}{x}\leq 0$ ?

$x<0$

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/inverse-signes.png)

##### Quelles sont les solutions de $\frac{1}{x}\leq a$ (avec $a>0$) ?

$x\in\left]-\infty;0\right[\cup\left[\frac{1}{a};+\infty\right[$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/inverse-inferieur-positif.png)

##### Quelles sont les solutions de $\frac{1}{x}\geq a$ (avec $a<0$) ?

$x\in \left]-\infty; \frac{1}{a}\right] \cup \left]0 ; +\infty\right[$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/inverse-superieur-negatif.png)

##### Quelles sont les solutions de $\frac{1}{x}\geq 0$ ?

$x>0$

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/inverse-signes.png)

##### Quelles sont les solutions de $\frac{1}{x}\geq a$ (avec $a>0$) ?

$x\in\left]0;\frac{1}{a}\right]$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/inverse-superieur-positif.png)

#### Subdeck: Fonction cube

##### Comment s'appelle la fonction définie par $x\mapsto x^3$ ?

Fonction cube

##### Quelle est l'expression de la fonction cube ?

$x\mapsto x^3$

##### Quel est le domaine de définition de la fonction cube ?

$\mathbb{R}$

##### Comment s'appelle la fonction représentée ici ?

![représentation](fonctions-de-reference/cube.png)

---

Fonction cube.

##### Quelle est la représentation graphique de la fonction cube ?

![représentation](fonctions-de-reference/cube.png)

##### Quel est le tableau de variations de la fonction cube ?

![variations](fonctions-de-reference/cube-variations.png)

##### Quel est le tableau de signes de la fonction cube ?

![signes](fonctions-de-reference/cube-signes.png)

##### À quelles conditions sur $a$ l'équation $x^3=a$ a-t-elle des solutions ?

L'équation $x^3=a$ a toujours des solutions, quelle que soit la valeur de $a$.

##### Quelles sont les solutions de $x^3=a$ ?

$x=\sqrt[3]{a}$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/cube-egale-nombre.png)

##### Quelles sont les solutions de $x^3\leq a$ ?

$x\leq\sqrt[3]{a}$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/cube-inferieur-nombre.png)

##### Quelles sont les solutions de $x^3\geq a$ ?

$x\geq\sqrt[3]{a}$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/cube-superieur-nombre.png)

#### Subdeck: Fonction racine carrée

##### Comment s'appelle la fonction définie par $x\mapsto \sqrt{x}$ ?

Fonction racine carrée

##### Quelle est l'expression de la fonction racine carrée ?

$x\mapsto \sqrt{x}$

##### Quel est le domaine de définition de la fonction racine carrée ?

$\mathbb{R}^+$, c'est-à-dire $\left[0;+\infty\right[$, ou tous les nombres réels positifs ou nuls.

##### Comment s'appelle la fonction représentée ici ?

![représentation](fonctions-de-reference/racine.png)

---

Fonction racine carrée.

##### Quelle est la représentation graphique de la fonction racine carrée ?

![représentation](fonctions-de-reference/racine.png)

##### Quel est le tableau de variations de la fonction racine carrée ?

![variations](fonctions-de-reference/racine-variations.png)

##### Quel est le tableau de signes de la fonction racine carrée ?

![signes](fonctions-de-reference/racine-signes.png)

##### À quelles conditions sur $a$ l'équation $\sqrt{x}=a$ a-t-elle des solutions ?

Si $a\geq0$.

##### Quelles sont les solutions de $\sqrt{x}=a$ (avec $a<0$) ?

Pas de solutions.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/racine-egale-negatif.png)

##### Quelles sont les solutions de $\sqrt{x}=0$ ?

Une seule solution : $x=0$.

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/racine-signes.png)

##### Quelles sont les solutions de $\sqrt{x}=a$ (avec $a>0$) ?

Une seule solution : $x=a^2$.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/racine-egale-positif.png)

##### Quelles sont les solutions de $\sqrt{x}\leq a$ (avec $a<0$) ?

Pas de solutions.

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/racine-egale-negatif.png)

##### Quelles sont les solutions de $\sqrt{x}\leq 0$ ?

Une seule solution : $x=0$.

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/racine-signes.png)

##### Quelles sont les solutions de $\sqrt{x}\leq a$ (avec $a>0$) ?

$x\in\left[0;a^2\right]$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/racine-inferieur-positif.png)

##### Quelles sont les solutions de $\sqrt{x}\geq a$ (avec $a<0$) ?

$x\in\left[0;+\infty\right[$ ou $x\in\mathbb{R}^+$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/racine-superieur-negatif.png)

##### Quelles sont les solutions de $\sqrt{x}\geq 0$ ?

$x\in\left[0;+\infty\right[$ ou $x\in\mathbb{R}^+$

On pouvait trouver la réponse en regardant le tableau de signes :

![signes](fonctions-de-reference/racine-signes.png)

##### Quelles sont les solutions de $\sqrt{x}\geq a$ (avec $a>0$) ?

$x\in\left[a^2;+\infty\right[$ ou $x\geq a^2$

On pouvait trouver la réponse avec le schéma suivant :

![signes](fonctions-de-reference/racine-superieur-positif.png)
