# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Polynômes du second degré

#### Subdeck: Racines

##### Quelle est la formule du discriminant d'un polynôme $ax^2+bx+c$ ?

$\Delta = b^2-4ac$

##### Que représente la formule $b^2-4ac$ pour un polynôme $ax^2+bx+c$ ?

Il s'agit du discriminant $\Delta$ du polynôme.

##### Combien de racines a le polynôme $ax^2+bx+c$ en fonction du discriminant $\Delta$ ?

- Si $\Delta<0$, il n'y a aucune racines.
- Si $\Delta=0$, il y a exactement une racine, dite racine double.
- Si $\Delta>0$, il y a deux racines.

##### Quelle est l'expression des racines du polynôme $ax^2+by+c$, dans le cas où $\Delta<0$ ?

Ce polynôme n'a pas de racines.

##### Quelle est l'expression des racines du polynôme $ax^2+by+c$, dans le cas où $\Delta=0$ ?

Ce polynôme a une racine double $x=-\frac{b}{2a}$.

##### Quelle est l'expression des racines du polynôme $ax^2+by+c$, dans le cas où $\Delta>0$ ?

Ce polynôme a deux racines $x_1=\frac{-b-\sqrt{\Delta}}{2a}$ et $x_2=\frac{-b+\sqrt{\Delta}}{2a}$.

##### Comment se factorise le polynôme $ax^2+bx+c$ en fonction de $\Delta$ ?

- Si $\Delta<0$, il ne se factorise pas.
- Si $\Delta=0$, il se factorise en $a\left(x-x_1\right)^2$, où $x_1$ est la racine double du polynôme.
- Si $\Delta>0$, il se factorise en $a\left(x-x_1\right)\left(x-x_2\right)$, où $x_1$ et $x_2$ sont les deux racines du polynôme.

#### Subdeck: Signe

##### Quel est le tableau de signes du polynôme $ax^2+bx+c$, dans le cas où $\Delta<0$ et $a<0$ ?

![signe](polynômes-du-second-degré/signe-delta-negatif-a-negatif.png)

##### Quel est le tableau de signes du polynôme $ax^2+bx+c$, dans le cas où $\Delta=0$ et $a<0$ ?

![signe](polynômes-du-second-degré/signe-delta-nul-a-negatif.png)

##### Quel est le tableau de signes du polynôme $ax^2+bx+c$, dans le cas où $\Delta>0$ et $a<0$ ?

![signe](polynômes-du-second-degré/signe-delta-positif-a-negatif.png)

##### Quel est le tableau de signes du polynôme $ax^2+bx+c$, dans le cas où $\Delta<0$ et $a>0$ ?

![signe](polynômes-du-second-degré/signe-delta-negatif-a-positif.png)

##### Quel est le tableau de signes du polynôme $ax^2+bx+c$, dans le cas où $\Delta=0$ et $a>0$ ?

![signe](polynômes-du-second-degré/signe-delta-nul-a-positif.png)

##### Quel est le tableau de signes du polynôme $ax^2+bx+c$, dans le cas où $\Delta>0$ et $a>0$ ?

![signe](polynômes-du-second-degré/signe-delta-positif-a-positif.png)

#### Subdeck: Interprétation graphique

##### Quels sont les signes de $\Delta$ et $a$ pour la fonction définie par $x\mapsto ax^2+bx+c$, et représentée ci-dessous ?

![interprétation graphique](polynômes-du-second-degré/courbe-delta-positif-a-positif.png)

---

$\Delta>0$ et $a>0$

##### Quels sont les signes de $\Delta$ et $a$ pour la fonction définie par $x\mapsto ax^2+bx+c$, et représentée ci-dessous ?

![interprétation graphique](polynômes-du-second-degré/courbe-delta-nul-a-positif.png)

---

$\Delta=0$ et $a>0$

##### Quels sont les signes de $\Delta$ et $a$ pour la fonction définie par $x\mapsto ax^2+bx+c$, et représentée ci-dessous ?

![interprétation graphique](polynômes-du-second-degré/courbe-delta-negatif-a-positif.png)

---

$\Delta<0$ et $a>0$

##### Quels sont les signes de $\Delta$ et $a$ pour la fonction définie par $x\mapsto ax^2+bx+c$, et représentée ci-dessous ?

![interprétation graphique](polynômes-du-second-degré/courbe-delta-positif-a-negatif.png)

---

$\Delta>0$ et $a<0$

##### Quels sont les signes de $\Delta$ et $a$ pour la fonction définie par $x\mapsto ax^2+bx+c$, et représentée ci-dessous ?

![interprétation graphique](polynômes-du-second-degré/courbe-delta-nul-a-negatif.png)

---

$\Delta=0$ et $a<0$

##### Quels sont les signes de $\Delta$ et $a$ pour la fonction définie par $x\mapsto ax^2+bx+c$, et représentée ci-dessous ?

![interprétation graphique](polynômes-du-second-degré/courbe-delta-negatif-a-negatif.png)

---

$\Delta<0$ et $a<0$

##### Tracer l'allure de la courbe de la fonction $f:x\mapsto ax^2+bx+c$ sachant que $\Delta>0$ et $a>0$.

![interprétation graphique](polynômes-du-second-degré/courbe-delta-positif-a-positif.png)

##### Tracer l'allure de la courbe de la fonction $f:x\mapsto ax^2+bx+c$ sachant que $\Delta=0$ et $a>0$.

![interprétation graphique](polynômes-du-second-degré/courbe-delta-nul-a-positif.png)

##### Tracer l'allure de la courbe de la fonction $f:x\mapsto ax^2+bx+c$ sachant que $\Delta<0$ et $a>0$.

![interprétation graphique](polynômes-du-second-degré/courbe-delta-negatif-a-positif.png)

##### Tracer l'allure de la courbe de la fonction $f:x\mapsto ax^2+bx+c$ sachant que $\Delta>0$ et $a<0$.

![interprétation graphique](polynômes-du-second-degré/courbe-delta-positif-a-negatif.png)

##### Tracer l'allure de la courbe de la fonction $f:x\mapsto ax^2+bx+c$ sachant que $\Delta=0$ et $a<0$.

![interprétation graphique](polynômes-du-second-degré/courbe-delta-nul-a-negatif.png)

##### Tracer l'allure de la courbe de la fonction $f:x\mapsto ax^2+bx+c$ sachant que $\Delta<0$ et $a<0$.

![interprétation graphique](polynômes-du-second-degré/courbe-delta-negatif-a-negatif.png)
