# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Produit scalaire

#### Subdeck: Expressions

##### Quelle est l'expression du produit scalaire $\overrightarrow{u}.\overrightarrow{v}$ utilisant le projeté orthogonal (avec $\overrightarrow{v}\neq\overrightarrow{0}$) ?

$\overrightarrow{u}.\overrightarrow{v}=\overrightarrow{w}.\overrightarrow{v}$, où $\overrightarrow{w}$ est le projeté orthogonal de $\overrightarrow{u}$ sur la droite de direction $\overrightarrow{v}$.

##### Quelle est l'expression du produit scalaire $\overrightarrow{u}.\overrightarrow{v}$ utilisant le cosinus (si aucun des deux vecteurs n'est nul) ?

$\overrightarrow{u}.\overrightarrow{v}=\lVert \overrightarrow{u}\rVert\times\lVert \overrightarrow{v}\rVert\times\cos(\overrightarrow{u};\overrightarrow{v})$

##### Quelle est l'expression du produit scalaire $\overrightarrow{u}.\overrightarrow{v}$ utilisant les coordonnées ?

En notant les coordonnées $\overrightarrow{u}(x;y)$ et $\overrightarrow{v}(x';y')$ dans un repère orthonormé :

$$\overrightarrow{u}.\overrightarrow{v}=x\times x'+y\times y'$$

##### À quelles conditions peut-on appliquer l'expression du produit scalaire utilisant le cosinus ?

Si aucun des deux vecteurs n'est nul.

##### À quelles conditions peut-on appliquer l'expression du produit scalaire utilisant le projeté orthogonal ?

Si le vecteur sur lequel l'autre vecteur est projeté n'est pas nul.

##### À quelles conditions peut-on appliquer l'expression du produit scalaire utilisant les coordonnées ?

Si les coordonnées sont données dans un repère orthonormé.

##### Quelle est l'expression du produit scalaire $\overrightarrow{AB}.\overrightarrow{AC}$ utilisant le projeté orthogonal (avec $A$ et $B$ disjoints) ?

$\overrightarrow{AB}.\overrightarrow{AC}=\overrightarrow{AB}.\overrightarrow{AH}$, où $H$ est le projet orthogonal de $C$ sur la droite $(AB)$.

##### Quelle est l'expression du produit scalaire $\overrightarrow{AB}.\overrightarrow{AC}$ utilisant le cosinus (avec $A$, $B$, $C$ deux à deux disjoints) ?

$\overrightarrow{AB}.\overrightarrow{AC}=AB\times AC\times\cos(\widehat{BAC})$

#### Subdeck: Colinéarité et Orthogonalité

##### Définir deux vecteurs *orthogonaux* (dans le plan) ?

Deux vecteurs du plan $\overrightarrow{AB}$ et $\overrightarrow{CD}$ sont orthogonaux si les droites $(AB)$ et $(CD)$ sont perpendiculaires.

##### Combien vaut $\overrightarrow{u}.\overrightarrow{0}$ ?

$$\overrightarrow{u}.\overrightarrow{0}=0$$

##### Quel est le lien entre produit scalaire et orthogonalité ?

Deux vecteurs non nuls sont orthogonaux si et seulement si leur produit scalaire est nul.

##### Quel est le lien entre produit scalaire et perpendicularité ?

Deux droites $(AB)$ et $(CD)$ sont perpendiculaires si et seulement si le produit scalaire $\overrightarrow{AB}.\overrightarrow{CD}$ est nul.

##### Que peut-on déduire si $\overrightarrow{u}.\overrightarrow{v}=0$ ? Deux possibilités…

- Au moins l'un des deux vecteurs est nul,
- ou les deux vecteurs sont orthogonaux.

##### Exprimer $\overrightarrow{u}.\overrightarrow{v}$ sans vecteurs, dans le cas où les deux vecteurs sont colinéaires.

- $\overrightarrow{u}.\overrightarrow{v}= \lVert \overrightarrow{u}\rVert\times\lVert \overrightarrow{v}\rVert$ si les deux vecteurs ont le même sens ;
- $\overrightarrow{u}.\overrightarrow{v}=-\lVert \overrightarrow{u}\rVert\times\lVert \overrightarrow{v}\rVert$ si les deux vecteurs sont de sens opposé.

##### Exprimer $\overrightarrow{AB}.\overrightarrow{CD}$ sans vecteurs, dans le cas où les deux droites $(AB)$ et $(CD)$ sont parallèles.

- $\overrightarrow{AB}.\overrightarrow{CD}= AB\times CD$ si les deux vecteurs ont le même sens ;
- $\overrightarrow{AB}.\overrightarrow{CD}=-AB\times CD$ si les deux vecteurs sont de sens opposé.

#### Subdeck: Règles de calcul

##### Développer $\left(\overrightarrow{u}+\overrightarrow{v}\right)^2$

$$\left(\overrightarrow{u}+\overrightarrow{v}\right)^2=\lVert \overrightarrow{u}\rVert^2 + 2\overrightarrow{u}.\overrightarrow{v} + \lVert \overrightarrow{v}\rVert^2$$

##### Développer $\left(\overrightarrow{u}-\overrightarrow{v}\right)^2$

$$\left(\overrightarrow{u}-\overrightarrow{v}\right)^2=\lVert \overrightarrow{u}\rVert^2 - 2\overrightarrow{u}.\overrightarrow{v} + \lVert \overrightarrow{v}\rVert^2$$

##### Développer $\left(\overrightarrow{u}+\overrightarrow{v}\right).\left(\overrightarrow{u}-\overrightarrow{v}\right)$

$$\left(\overrightarrow{u}+\overrightarrow{v}\right).\left(\overrightarrow{u}-\overrightarrow{v}\right)=\lVert \overrightarrow{u}\rVert^2 - \lVert \overrightarrow{v}\rVert^2$$

##### Factoriser $\lVert \overrightarrow{u}\rVert^2 + 2\overrightarrow{u}.\overrightarrow{v} + \lVert \overrightarrow{v}\rVert^2$

$$\lVert \overrightarrow{u}\rVert^2 + 2\overrightarrow{u}.\overrightarrow{v} + \lVert \overrightarrow{v}\rVert^2 = \left(\overrightarrow{u}+\overrightarrow{v}\right)^2$$

##### Factoriser $\lVert \overrightarrow{u}\rVert^2 - 2\overrightarrow{u}.\overrightarrow{v} + \lVert \overrightarrow{v}\rVert^2$

$$\lVert \overrightarrow{u}\rVert^2 - 2\overrightarrow{u}.\overrightarrow{v} + \lVert \overrightarrow{v}\rVert^2 = \left(\overrightarrow{u}-\overrightarrow{v}\right)^2$$

##### Factoriser $\lVert \overrightarrow{u}\rVert^2 - \lVert \overrightarrow{v}\rVert^2$

$$\lVert \overrightarrow{u}\rVert^2 - \lVert \overrightarrow{v}\rVert^2 = \left(\overrightarrow{u}+\overrightarrow{v}\right).\left(\overrightarrow{u}-\overrightarrow{v}\right)$$

##### Le produit scalaire est commutatif, c'est-à-dire :

$$\overrightarrow{u}.\overrightarrow{v}= \overrightarrow{v}.\overrightarrow{u}$$

##### Développer $\overrightarrow{u}.\left(\overrightarrow{v}+\overrightarrow{w}\right)$

$$\overrightarrow{u}.\left(\overrightarrow{v}+\overrightarrow{w}\right)=\overrightarrow{u}.\overrightarrow{v}+\overrightarrow{u}.\overrightarrow{w}$$

##### Factoriser $\overrightarrow{u}.\overrightarrow{v}+\overrightarrow{u}.\overrightarrow{w}$

$$\overrightarrow{u}.\overrightarrow{v}+\overrightarrow{u}.\overrightarrow{w}=\overrightarrow{u}.\left(\overrightarrow{v}+\overrightarrow{w}\right)$$

##### Compléter : $\left(k\overrightarrow{u}\right).\overrightarrow{v}=$

$$\left(k\overrightarrow{u}\right).\overrightarrow{v}=k\left(\overrightarrow{u}.\overrightarrow{v}\right)=\overrightarrow{u}.\left(k\overrightarrow{v}\right)$$

#### Subdeck: Triangle

##### Énoncer le théorème d'Al-Kashi

Soit un triangle $ABC$ quelconque. Alors :

$$BC^2=AB^2+AC^2-2\times AB\times AC\times\cos\widehat{BAC}$$
