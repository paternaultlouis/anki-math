# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Probabilités conditionnelles

#### Subdeck: Probabilités conditionnelles

##### Comment se lit $P_A(B)$ ?

Probabilité de $B$ sachant $A$.

##### Comment se note la probabilité de $B$ sachant $A$ (où $A$ et $B$ sont deux évènements) ?

$$P_A(B)$$

##### Soient $A$ et $B$ deux évènements, tels que $P(A)\neq0$. Compléter : $$P_…(…)+P_…(…)=1$$

$$P_A(B)+P_A(\overline B)=1$$

##### Soient $A$ et $B$ deux évènements, tels que $P(A)\neq0$. Compléter en faisant intervenir $P_A(B)$ : $$P(A\cap B)=…$$

$$P(A\cap B)=P(A)\times P_A(B)$$

##### Soient $A$ et $B$ deux évènements, tels que $P(A)\neq0$. Compléter en faisant intervenir $P(A\cap B)$ : $$P_A(B)=…$$

$$P_A(B) = \frac{P(A\cap B)}{P(A)}$$

##### Que peut-on dire des évènements $A$ et $B$ si $P(A\cap B)=P(A)\times P(B)$ ?

Les évènements sont indépendants.

##### Compléter : deux évènements $A$ et $B$ sont indépendants si $$P(A\cap B)=…$$

$$P(A\cap B)=P(A)\times P(B)$$

##### Compléter : Les évènements $A$ et $B$ sont indépendants si et seulement si $$P_A(B)=…$$

$$P_A(B)=P(B)$$

#### Subdeck: Probabilités totales

##### Dans un arbre de probabilités, la somme des probabilités des branches issues de chaque nœud de l'arbre est :

![arbre-somme-branches](probabilités-conditionnelles/arbre-somme-branches.png)

---

… égale à 1. Par exemple : $a+b+c=1$.

##### Dans un arbre de probabilités, comment calcule-t-on la probabilité d'un évènement à l'extrémité d'un chemin ?

![arbre-proba-chemin](probabilités-conditionnelles/arbre-proba-chemin.png)

---

En multipliant toutes les probabilités des branches composant ce chemin. Par exemple :

$$P(M\cap A\cap \overline{W})=m\times a \times w$$

##### Dans un arbre de probabilités, comment calcule-t-on la probabilité d'un évènement composé de plusieurs chemins ?

![arbre-proba-evenement](probabilités-conditionnelles/arbre-proba-evenement.png)

---

En ajoutant les probabilités des évènements qui le composent. Par exemple :

$$P(S)=P(S\cap A)+P(S\cap B)+P(S\cap C)$$

##### Soit $A$ un évènement d'un univers $\Omega$. Les évènements $A_1$, $A_2$, …, $A_n$ forment une partition de $A$ si :

- les évènements sont deux à deux disjoints : $A_i\cap A_j=\emptyset$ pour tous nombres $i$ et $j$ ;
- l'union des évènements $A_1$, $A_2$, …, $A_n$ est égale à $A$ : $A_1\cup A_2\cup …\cup A_n=A$.

##### Soient $B$ un évènement d'un univers $\Omega$, et $A_1$, $A_2$, …, $A_n$ une partition de $\Omega$. Alors $$P(B)=…$$

$$P(B)=P(B\cap A_1)+P(B\cap A_2)+…+P(B\cap A_n)$$
