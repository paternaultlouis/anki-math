# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Géométrie repérée

#### Subdeck: Droites

##### À quelles conditions sur les nombres $a$, $b$, $c$ l'équation $ax+by+c=0$ définit-elle une droite ?

Si au moins un des deux nombres $a$ ou $b$ est non nul.

##### Comment s'appelle l'équation d'une droite sous la forme $ax+by+c=0$ ?

Équation cartésienne.

##### Quelle est la forme de l'équation cartésienne d'une droite ?

$ax+by+c=0$, avec $a$ ou $b$ non nul.

##### Qu'est-ce qu'un vecteur directeur d'une droite ?

C'est n'importe quel vecteur $\overrightarrow{AB}$, où $A$ et $B$ sont deux points distincts de la droite.

##### Soient deux points $A$ et $B$ distincts d'une droite. Que peut-on dire du vecteur $\overrightarrow{AB}$ ?

C'est un vecteur directeur à la droite.

##### Donner les coordonnées d'un vecteur directeur de la droite d'équation cartésienne $ax+by+c=0$

Le vecteur de coordonnées $(-b;a)$ est directeur à la droite.

##### Soit une droite d'équation cartésienne $ax+by+c=0$. Que peut-on dire du vecteur de coordonnées $(-b;a)$ ?

C'est un vecteur directeur de la droite.

##### Qu'est-ce qu'un vecteur normal à une droite ?

C'est n'importe quel vecteur orthogonal à un vecteur directeur de la droite.

##### Soit un vecteur $\overrightarrow{n}$ orthogonal à un vecteur directeur d'une droite. Que peut-on dire de $\overrightarrow{n}$ ?

C'est un vecteur normal à la droite.

##### Donner les coordonnées d'un vecteur normal de la droite d'équation cartésienne $ax+by+c=0$

Le vecteur de coordonnées $(a;b)$ est normal à la droite.

##### Soit une droite d'équation cartésienne $ax+by+c=0$. Que peut-on dire du vecteur de coordonnées $(a;b)$ ?

C'est un vecteur normal de la droite.

##### Donner deux propositions équivalentes à « Deux droites sont parallèles » en utilisant les vecteurs directeurs.

Soient deux droites du plan. Les propositions suivantes sont équivalentes :

- Les deux droites sont parallèles.
- Les deux droites ont deux vecteurs directeurs colinéaires.
- Les deux droites ont tous leurs vecteurs directeurs colinéaires.

##### Donner deux propositions équivalentes à « Deux droites sont parallèles » en utilisant les vecteurs normaux.

Soient deux droites du plan. Les propositions suivantes sont équivalentes :

- Les deux droites sont parallèles.
- Les deux droites ont deux vecteurs normaux colinéaires.
- Les deux droites ont tous leurs vecteurs normaux colinéaires.

##### Donner deux propositions équivalentes à « Deux droites sont parallèles » en utilisant à la fois les vecteurs normaux et directeurs.

- Les deux droites sont parallèles.
- Un vecteur normal d'une droite est orthogonal à un vecteur directeur de l'autre droite.
- Chacun des vecteurs normaux d'une droite est orthogonal à chacun des vecteurs directeurs de l'autre droite.

##### Donner trois propositions équivalentes à « Deux droites sont perpendiculaires », en utilisant les vecteurs normaux ou directeurs.

Soient deux droites du plan. Les propositions suivantes sont équivalentes.

- Les droites sont perpendiculaires.
- Un vecteur normal de l'une et un vecteur directeur de l'autre sont colinéaires.
- Leurs vecteurs directeurs sont orthogonaux.
- Leurs vecteurs normaux sont orthogonaux.

##### Quelle méthode permet de déterminer une équation cartésienne de la droite passant par le point $A$ et de vecteur normal $\overrightarrow{n}$, dont on connait les coordonnées ?

1. Pour tout point du plan $M(x;y)$, on exprime les coordonnées de $\overrightarrow{AM}$ en fonction de $x$ et $y$.
2. Le point $M$ est sur la droite si et seulement si le produit scalaire $\overrightarrow{AM}.\overrightarrow{n}$ est nul.
3. On développe le produit scalaire dans l'équation $\overrightarrow{AM}.\overrightarrow{n}=0$, et on obtient l'équation cartésienne de la droite.

##### Quelle méthode permet de déterminer les coordonnées du projeté orthogonal d'un point $A$ (de coordonnées connues) sur une droite $d$ définie par son équation cartésienne ?

1. On considère la droite $d_H$, perpendiculaire à $d$ et passant par $A$ : l'intersection de $d$ et $d_H$ est le projeté orthogonal recherché.
2. On détermine un vecteur directeur de la droite $d$ : c'est un vecteur normal de $d_H$.
3. On détermine une équation cartésienne de la droite $d_H$ (sachant qu'elle passe par $A$, et qu'un vecteur normal a été trouvé à la question précédente).
4. On résout le système d'équation composé des deux équations cartésiennes de droites : le couple de solutions donne les coordonnées du projeté orthogonal recherché.

#### Subdeck: Cercles

##### Quelle est la définition d'un cercle (dans le plan) ?

Dans un plan, le cercle de centre $A$ et de rayon $r$ strictement positif est l'ensemble des points $M$ du plan situés à une distance $r$ de $A$.

##### Étant donné un point $A$ du plan, et un nombre $r$ strictement positif, comment appelle-t-on l'ensemble des points $M$ du plan situés à une distance $r$ de $A$ ?

Il s'agit du cercle de centre $A$ et de rayon $r$.

##### Dans le plan muni d'un repère orthonormé, quelle est l'équation cartésienne du cercle de centre $A(x_A; y_A)$ et de rayon $r$ ?

$$\left(x-x_A\right)^2+\left(y-y_A\right)^2=r^2$$

##### Dans le plan muni d'un repère orthonormé, quel est le lieu géométrique défini par l'équation $\left(x-x_A\right)^2+\left(y-y_A\right)^2=r^2$ (où $r$ est un nombre strictement positif) ?

Il s'agit du cercle de centre $A(x_A; y_A)$ et de rayon $r$.

##### Étant donnés deux points distincts $A$ et $B$ du plan muni d'un repère orthonormé, quel est le lieu géométrique défini par l'ensemble des points $M$ vérifiant $\overrightarrow{AM}.\overrightarrow{BM}=0$ ?

Il s'agit du cercle de diamètre $[AB]$.

##### Dans le plan muni d'un repère orthonormé, quelle équation utilisant des vecteurs caractérise le cercle de diamètre $[AB]$ ?

Ce cercle est l'ensemble des points $M$ du plan vérifiant :

$$\overrightarrow{AM}.\overrightarrow{BM}=0$$
