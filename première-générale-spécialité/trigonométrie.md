# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Trigonométrie

#### Subdeck: Radian & Cercle trigonométrique

##### Qu'est-ce que la mesure d'un angle en *radians* ?

La mesure d'un angle en radians est la longueur de l'arc du cercle de rayon 1 interprété par cet angle.

![radian](trigonométrie/radian.png)

##### Quelle est la relation entre les mesures en radians et degrés d'un même angle ?

Elles sont proportionnelles.

##### Combien vaut $2\pi$ radians en degré ?

360°

##### Combien vaut $\pi$ radians en degré ?

180°

##### Combien vaut 360° en radians ?

$2\pi$

##### Combien vaut 180° en radians ?

$\pi$

##### Qu'est-ce que le sens trigonométrique (ou direct) d'un angle ?

C'est le sens inverse des aiguilles d'une montre.

##### Que peut-on dire de la mesure d'un angle orienté en sens direct ?

Cette mesure est positive.

##### Que peut-on dire de la mesure d'un angle orienté en sens indirect ?

Cette mesure est négative.

##### Qu'est-ce que le *cercle trigonométrique* ?

C'est le cercle :

- de centre $O$ (origine du repère) ;
- de rayon 1 ;
- orienté dans le sens direct.

![cercle trigo](trigonométrie/cercle-trigo.png)

#### Subdeck: Sinus et Cosinus

##### Quelle est la définition du *sinus* d'un nombre $t$ ?

Le *sinus* d'un nombre $t$ est l'ordonnée du point $M$, image de $t$ sur le cercle trigonométrique.

![sinus](trigonométrie/sinus-cosinus.png)

##### Quelle est la définition du *cosinus* d'un nombre $t$ ?

Le *cosinus* d'un nombre $t$ est l'abscisse du point $M$, image de $t$ sur le cercle trigonométrique.

![cosinus](trigonométrie/sinus-cosinus.png)

##### Compléter l'égalité en faisant intervenir les sinus et cosinus d'un nombre $t$ quelconque : $...=1$.

$\sin^2 t + \cos^2 t = 1$

##### Quelles sont les valeurs minimales et maximales des sinus et cosinus ?

Pour tout nombre $t$ réel :

$$
\left\{\begin{array}{c}
-1 \leq \sin t \leq 1 \\
-1 \leq \cos t \leq 1 \\
\end{array}\right.
$$

##### Subdeck: Valeurs remarquables

###### $\cos 0 = ?$

$\cos 0 = 1$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\cos \frac{\pi}{6} = ?$

$\cos \frac{\pi}{6} = \frac{\sqrt{3}}{2}$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\cos \frac{\pi}{4} = ?$

$\cos \frac{\pi}{4} = \frac{\sqrt{2}}{2}$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\cos \frac{\pi}{3} = ?$

$\cos \frac{\pi}{3} = \frac{1}{2}$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\cos \frac{\pi}{2} = ?$

$\cos \frac{\pi}{2} = 0$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\sin 0 = ?$

$\sin 0 = 0$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\sin \frac{\pi}{6} = ?$

$\sin \frac{\pi}{6} = \frac{1}{2}$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\sin \frac{\pi}{4} = ?$

$\sin \frac{\pi}{4} = \frac{\sqrt{2}}{2}$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\sin \frac{\pi}{3} = ?$

$\sin \frac{\pi}{3} = \frac{\sqrt{3}}{2}$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### $\sin \frac{\pi}{2} = ?$

$\sin \frac{\pi}{2} = 1$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\cos \ldots = 1
$$

---

$$
\cos 0 = 1
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\cos \ldots = \frac{\sqrt{3}}{2}
$$

---

$$
\cos \frac{\pi}{6} = \frac{\sqrt{3}}{2}
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\cos \ldots = \frac{\sqrt{2}}{2}
$$

---

$$
\cos \frac{\pi}{4} = \frac{\sqrt{2}}{2}
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\cos \ldots = \frac{1}{2}
$$

---

$$
\cos \frac{\pi}{3} = \frac{1}{2}
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\cos \ldots = 0
$$

---

$$
\cos \frac{\pi}{2} = 0
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\sin \ldots = 1
$$

---

$$
\sin \frac{\pi}{2} = 1
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\sin \ldots = \frac{\sqrt{3}}{2}
$$

---

$$
\sin \frac{\pi}{3} = \frac{\sqrt{3}}{2}
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\sin \ldots = \frac{\sqrt{2}}{2}
$$

---

$$
\sin \frac{\pi}{4} = \frac{\sqrt{2}}{2}
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\sin \ldots = \frac{1}{2}
$$

---

$$
\sin \frac{\pi}{6} = \frac{1}{2}
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)

###### Compléter avec un nombre entre $0$ et $\frac{\pi}{2}$ :

$$
\sin \ldots = 0
$$

---

$$
\sin 0 = 0
$$

![valeurs-remarquables](trigonométrie/valeurs-remarquables.png)
