# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Généralités sur les fonctions

#### Subdeck: Variations

##### Définition : Une fonction $f$ est *croissante* sur un intervalle $I$ si…

pour tous nombres $a$ et $b$ de $I$, si $a < b$, alors $f(a) \leq f(b)$.

##### Définition : Une fonction $f$ est *strictement croissante* sur un intervalle $I$ si…

pour tous nombres $a$ et $b$ de $I$, si $a < b$, alors $f(a) < f(b)$.

##### Définition : Une fonction $f$ est *décroissante* sur un intervalle $I$ si…

pour tous nombres $a$ et $b$ de $I$, si $a < b$, alors $f(a) \geq f(b)$.

##### Définition : Une fonction $f$ est *strictement décroissante* sur un intervalle $I$ si…

pour tous nombres $a$ et $b$ de $I$, si $a < b$, alors $f(a) > f(b)$.

##### Définition : Une fonction $f$ est *constante* sur un intervalle $I$ si…

pour tous nombres $a$ et $b$ de $I$, on a $f(a) = f(b)$.

##### Définition : Une fonction $f$ est *monotone* sur un intervalle $I$ si…

cette fonction est croissante sur $I$ ou décroissante sur $I$.

##### Définition : Une fonction $f$ est *strictement monotone* sur un intervalle $I$ si…

cette fonction est strictement croissante sur $I$ ou strictement décroissante sur $I$.

#### Subdeck: Bestiaire

##### Subdeck: Fonction carré

###### Comment s'appelle la fonction définie par $x\mapsto x^2$ ?

Fonction carré

###### Quelle est l'expression de la fonction carré ?

$x\mapsto x^2$

###### Quel est le domaine de définition de la fonction carré ?

$\mathbb{R}$

###### Comment s'appelle la fonction représentée ici ?

![représentation](généralités-sur-les-fonctions/carre.png)

---

Fonction carré.

###### Quelle est la représentation graphique de la fonction carré ?

![représentation](généralités-sur-les-fonctions/carre.png)

###### Quel est le tableau de variations de la fonction carré ?

![variations](généralités-sur-les-fonctions/carre-variations.png)

###### Quel est le tableau de signes de la fonction carré ?

![signes](généralités-sur-les-fonctions/carre-signes.png)

##### Subdeck: Fonction inverse

###### Comment s'appelle la fonction définie par $x\mapsto \frac{1}{x}$ ?

Fonction inverse

###### Quelle est l'expression de la fonction inverse ?

$x\mapsto \frac{1}{x}$

###### Quel est le domaine de définition de la fonction inverse ?

Tous les nombres réels sauf zéro : $\mathbb{R}^*$

###### Comment s'appelle la fonction représentée ici ?

![représentation](généralités-sur-les-fonctions/inverse.png)

---

Fonction inverse.

###### Quelle est la représentation graphique de la fonction inverse ?

![représentation](généralités-sur-les-fonctions/inverse.png)

###### Quel est le tableau de variations de la fonction inverse ?

![variations](généralités-sur-les-fonctions/inverse-variations.png)

###### Quel est le tableau de signes de la fonction inverse ?

![signes](généralités-sur-les-fonctions/inverse-signes.png)

##### Subdeck: Fonction cube

###### Comment s'appelle la fonction définie par $x\mapsto x^3$ ?

Fonction cube

###### Quelle est l'expression de la fonction cube ?

$x\mapsto x^3$

###### Quel est le domaine de définition de la fonction cube ?

$\mathbb{R}$

###### Comment s'appelle la fonction représentée ici ?

![représentation](généralités-sur-les-fonctions/cube.png)

---

Fonction cube.

###### Quelle est la représentation graphique de la fonction cube ?

![représentation](généralités-sur-les-fonctions/cube.png)

###### Quel est le tableau de variations de la fonction cube ?

![variations](généralités-sur-les-fonctions/cube-variations.png)

###### Quel est le tableau de signes de la fonction cube ?

![signes](généralités-sur-les-fonctions/cube-signes.png)

##### Subdeck: Fonction racine carrée

###### Comment s'appelle la fonction définie par $x\mapsto \sqrt{x}$ ?

Fonction racine carrée

###### Quelle est l'expression de la fonction racine carrée ?

$x\mapsto \sqrt{x}$

###### Quel est le domaine de définition de la fonction racine carrée ?

$\mathbb{R}^+$, c'est-à-dire $\left[0;+\infty\right[$, ou tous les nombres réels positifs ou nuls.

###### Comment s'appelle la fonction représentée ici ?

![représentation](généralités-sur-les-fonctions/racine.png)

---

Fonction racine carrée.

###### Quelle est la représentation graphique de la fonction racine carrée ?

![représentation](généralités-sur-les-fonctions/racine.png)

###### Quel est le tableau de variations de la fonction racine carrée ?

![variations](généralités-sur-les-fonctions/racine-variations.png)

###### Quel est le tableau de signes de la fonction racine carrée ?

![signes](généralités-sur-les-fonctions/racine-signes.png)

##### Subdeck: Fonction valeur absolue

###### Comment s'appelle la fonction définie par $x\mapsto \left|x\right|$ ?

Fonction valeur absolue

###### Quelle est l'expression de la fonction valeur absolue ?

$x\mapsto \left|x\right|$, c'est-à-dire $x$ si $x\geq0$, et $-x$ sinon.

###### Quel est le domaine de définition de la fonction absolue ?

$\mathbb{R}$

###### Comment s'appelle la fonction représentée ici ?

![représentation](généralités-sur-les-fonctions/absolue.png)

---

Fonction valeur absolue

###### Quelle est la représentation graphique de la fonction valeur absolue ?

![représentation](généralités-sur-les-fonctions/absolue.png)

###### Quel est le tableau de variations de la fonction valeur absolue ?

![variations](généralités-sur-les-fonctions/absolue-variations.png)

###### Quel est le tableau de signes de la fonction valeur absolue ?

![signes](généralités-sur-les-fonctions/absolue-signes.png)

#### Subdeck: Fonctions polynômes du second degré

##### Qu'est-ce qu'une fonction polynôme du second degré ?

Une fonction pouvant s'écrire sous la forme $f:x\mapsto ax^2+bx+c$, où $a$, $b$, $c$ sont des nombres réels, et $a\neq0$.

##### À quelles conditions la fonction $f:x\mapsto ax^2+bx+c$ est-elle un polynôme du second degré ?

Si $a\neq0$.

##### Comment s'appelle la courbe d'un polynôme du second degré ?

Une parabole

##### Quels sont les axes et centres de symétrie de la courbe d'un polynôme du second degré $x\mapsto ax^2+bx+c$ ?

La courbe a une seul axe de symétrie, d'équation $x=-\frac{b}{2a}$.

##### Quelle est l'abscisse du sommet d'un polynôme du second degré $x\mapsto ax^2+bx+c$ ?

$-\frac{b}{2a}$

##### Comment appelle-t-on les solutions de $ax^2+bx+c=0$ ?

Les *racines* du polynôme $ax^2+bx+c$.

##### Qu'est-ce qu'une *racine* d'une polynôme $f$ ?

Les solutions de $f(x)=0$.

##### Comment appelle-t-on la forme $ax^2+bx+c$ d'un polynôme du second degré ?

La forme développée.

##### Quelle est la forme développée d'un polynôme du second degré ?

$ax^2+bx+c$

##### Comment appelle-t-on la forme $a(x-\alpha)^2+\beta$ d'un polynôme du second degré ?

La forme canonique.

##### Quelle est la forme canonique d'un polynôme du second degré ?

$a(x-\alpha)^2+\beta$

##### Comment appelle-t-on la forme $a(x-x_1)(x-x_2)$ d'un polynôme du second degré ?

La forme factorisée.

##### Quelle est la forme factorisée d'un polynôme du second degré ?

$a(x-x_1)(x-x_2)$

##### À quoi correspondent les valeurs $\alpha$ et $\beta$ dans la forme canonique $a(x-\alpha)^2+\beta$ d'un polynôme du second degré ?

$(\alpha ; \beta)$ sont les coordonnées du sommet de sa parabole.

##### À quoi correspondent les valeurs $x_1$ et $x_2$ dans la forme factorisée $a(x-x_1)(x-x_2)$ d'un polynôme du second degré ?

Aux racines du polynôme.

##### Placer $\alpha$ sur la courbe d'un polynôme du second degré

![parabole](généralités-sur-les-fonctions/parabole.png)

---

![parabole](généralités-sur-les-fonctions/parabole-alpha.png)

##### Placer $\beta$ sur la courbe d'un polynôme du second degré

![parabole](généralités-sur-les-fonctions/parabole.png)

---

![parabole](généralités-sur-les-fonctions/parabole-beta.png)

##### Placer $x_1$ et $x_2$ sur la courbe d'un polynôme du second degré

![parabole](généralités-sur-les-fonctions/parabole.png)

---

![parabole](généralités-sur-les-fonctions/parabole-racines.png)

##### Placer $c$ sur la courbe d'un polynôme du second degré

![parabole](généralités-sur-les-fonctions/parabole.png)

---

![parabole](généralités-sur-les-fonctions/parabole-c.png)

##### À quel paramètre d'un polynôme du second degré correspond la valeur suivante ?

![parabole](généralités-sur-les-fonctions/parabole-alpha-cache.png)

---

$\alpha$ dans la forme canonique $a(x-\alpha)^2+\beta$.

##### À quel paramètre d'un polynôme du second degré correspond la valeur suivante ?

![parabole](généralités-sur-les-fonctions/parabole-beta-cache.png)

---

$\beta$ dans la forme canonique $a(x-\alpha)^2+\beta$.

##### À quel paramètre d'un polynôme du second degré correspondent les valeurs suivante ?

![parabole](généralités-sur-les-fonctions/parabole-racines-cache.png)

---

Aux racines $x_1$ et $x_2$ (que l'on retrouve dans la forme factorisée $a(x-x_1)(x-x_2)$).

##### À quel paramètre d'un polynôme du second degré correspond la valeur suivante ?

![parabole](généralités-sur-les-fonctions/parabole-c-cache.png)

---

À $c$ dans la forme développée $ax^2+bx+c$.

##### Quelle est l'influence du paramètre $a$ sur la courbe d'un polynôme du second degré $ax^2+bx+c$ ?

Le signe de $a$ change « l'orientation » de la parabole :

![parabole](généralités-sur-les-fonctions/parabole-signe-de-a.png)

##### Quel est le tableau de variations du polynôme du second degré $x\mapsto ax^2+bx+c$, avec $a>0$ ?

![variations](généralités-sur-les-fonctions/polynome-variations-a-positif.png)

##### Quel est le tableau de variations du polynôme du second degré $x\mapsto ax^2+bx+c$, avec $a<0$ ?

![variations](généralités-sur-les-fonctions/polynome-variations-a-negatif.png)

##### Quelle est la relation entre les paramètres $x_1$, $x_2$, et $\alpha$ d'un polynôme du second degré ?

$\alpha=\frac{x_1+x_2}{2}$

##### Quelle est la relation entre les paramètres $x_1$, $x_2$, $a$ et $c$ d'un polynôme du second degré ?

$x_1\times x_2=\frac{c}{a}$

##### Quelle est la relation entre les paramètres $\alpha$ et $\beta$ dans un polynôme du second degré $f$ ?

$\beta=f(\alpha)$

##### Quelle est la relation entre les paramètres $\alpha$, $a$ et $b$ d'un polynôme du second degré ?

$\alpha=-\frac{b}{2a}$
