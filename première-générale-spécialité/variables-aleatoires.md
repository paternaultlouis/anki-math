# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Variables aléatoires

#### Définition

Une *variable aléatoire réelle* associée à une expérience aléatoire d'univers $\Omega$ est…

---

une fonction $X$ définie sur $\Omega$ et à valeur dans $\mathbb{R}$.

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, l'évènement $\left\{X=x\right\}$ est…

---

l'ensemble des issues de $\Omega$ auxquelles est associé le nombre réel $x$.

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, l'évènement $\left\{X<x\right\}$ est…

---

l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel strictement inférieur à $x$.

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, l'évènement $\left\{X>x\right\}$ est…

---

l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel strictement supérieur à $x$.

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, l'évènement $\left\{X\leq x\right\}$ est…

---

l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel inférieur ou égal à $x$.

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, l'évènement $\left\{X\geq x\right\}$ est…

---

l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel supérieur ou égal à $x$.

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, comment est noté l'évènement égal à l'ensemble des issues de $\Omega$ auxquelles est associé le nombre réel $x$ ?

---

$\left\{X=x\right\}$

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, comment est noté l'évènement égal à l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel strictement inférieur à $x$ ?

---

$\left\{X<x\right\}$

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, comment est noté l'évènement égal à l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel strictement supérieur à $x$ ?

---

$\left\{X>x\right\}$

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, comment est noté l'évènement égal à l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel inférieur ou égal à $x$ ?

---

$\left\{X\leq x\right\}$

#### Évènement

Étant donné une variable aléatoire $X$ définie sur un univers $\Omega$, comment est noté l'évènement égal à l'ensemble des issues de $\Omega$ auxquelles est associé un nombre réel supérieur ou égal à $x$ ?

---

$\left\{X\geq x\right\}$

#### La *probabilité* de l'évènement $\left\{X=x\right\}$ est…

la somme des probabilités des issues de $\Omega$ auxquelles on associe $x$ par la variable aléatoire $X$.

#### La *loi de probabilité* d'une variable aléatoire $X$ est…

la donnée de chacune des probabilités de l'évènement $\left\{X=x\right\}$, pour toutes les valeurs possibles de $x$.

#### Comment s'appelle le tableau donnant la probabilité de chacun des évènements $\left\{X=x\right\}$, pour toutes les valeurs possibles de $x$ ?

La *loi de probabilité* de $X$.

#### Donner la formule permettant de calculer l'espérance d'une variable aléatoire $X$

$$E(X)=x_1P(X=x_1)+x_2P(X=x_2)+\cdots+x_nP(X=x_n)$$

#### Que permet de calculer la formule suivante ?

$$x_1P(X=x_1)+x_2P(X=x_2)+\cdots+x_nP(X=x_n)$$

---

L'espérance $E(X)$ de la variable aléatoire $X$.

#### Comment se note l'espérance d'une variable aléatoire $X$ ?

$$E(X)$$

#### Quelle est la formule permettant de calculer la variance d'une variable aléatoire $X$ ?

$$V(X)=P(X=x_1)\times\left(x_1-E(X)\right)^2 + P(X=x_2)\times\left(x_2-E(X)\right)^2 + \cdots + P(X=x_n)\times\left(x_n-E(X)\right)^2$$

#### Que permet de calculer la formule suivante ?

$$P(X=x_1)\times\left(x_1-E(X)\right)^2 + P(X=x_2)\times\left(x_2-E(X)\right)^2 + \cdots + P(X=x_n)\times\left(x_n-E(X)\right)^2$$

---

La variance $V(X)$ de la variable aléatoire $X$.

#### Quelle est la formule permettant de calculer l'écart-type ?

$$\sigma(X)=\sqrt{V(X)}$$

#### Que permet de calculer la formule : $\sqrt{V(X)}$ ?

L'écart-type $\sigma(X)$ de la variable aléatoire $X$.

#### Comment est notée la variance d'une variable aléatoire $X$ ?

$$V(X)$$

#### Comment est noté l'écart-type d'une variable aléatoire $X$ ?

$$\sigma(X)$$

#### Que désigne $V(X)$ ?

La variance de la variable aléatoire $X$.

#### Que désigne $\sigma(X)$ ?

L'écart-type de la variable aléatoire $X$.
