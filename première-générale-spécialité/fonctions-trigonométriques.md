# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Trigonométrie

#### Subdeck: Fonctions trigonométriques

##### Donner les éléments de parité de la fonction sinus

La fonction sinus est impaire, mais elle n'est pas paire.

##### Donner les éléments de parité de la fonction cosinus

La fonction cosinus est paire, mais elle n'est pas impaire.

##### Quelle est la (plus petite) période des fonctions sinus et cosinus ?

Les fonctions sinus et cosinus sont périodes de période $2\pi$.

##### Quelles sont l'ensembles des périodes des fonctions sinus et cosinus ?

Les fonctions sinus et cosinus sont périodes de période $2k\pi$, pour $k\in\mathbb{Z}^*$.

##### Quelle st la fonction représentée ici ?

![sinus](fonctions-trigonométriques/sinus.png)

---

Fonction sinus.

##### Quelle st la fonction représentée ici ?

![cosinus](fonctions-trigonométriques/cosinus.png)

---

Fonction cosinus

##### Quels sont les éléments de symétrie de la courbe de la fonction sinus ?

La courbe de la fonction sinus est symétrique par rapport à l'origine du repère (mais elle a aussi d'autres centres et axes de symétrie).

##### Quels sont les éléments de symétrie de la courbe de la fonction cosinus ?

La courbe de la fonction sinus est symétrique par rapport à l'axe des ordonnées (mais elle a aussi d'autres centres et axes de symétrie).

##### Quel est le tableau de signes de la fonction sinus sur l'intervalle $[-\pi;\pi]$ ?

![signe-sinus](fonctions-trigonométriques/sinus-signes.png)

##### Quel est le tableau de signes de la fonction cosinus sur l'intervalle $[-\pi;\pi]$ ?

![signe-cosinus](fonctions-trigonométriques/cosinus-signes.png)

##### Quel est le tableau de variations de la fonction sinus sur l'intervalle $[-\pi;\pi]$ ?

![variations-sinus](fonctions-trigonométriques/sinus-variations.png)

##### Quel est le tableau de variations de la fonction cosinus sur l'intervalle $[-\pi;\pi]$ ?

![variations-cosinus](fonctions-trigonométriques/cosinus-variations.png)
