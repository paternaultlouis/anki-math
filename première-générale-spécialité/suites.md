# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Suites

#### Subdeck: Notion de suite

##### Compléter : Une ??? $u$ est une fonction définie sur $\mathbb{N}$ (ou une partie de $\mathbb{N}$) qui à tout entier $n$ (appelé rang) de son domaine de définition associe le réel $u_n$, appelé terme de la suite de rang $n$.

suite

##### Compléter : Une suite $u$ est une fonction définie sur ??? (ou une partie de ???) qui à tout entier $n$ (appelé rang) de son domaine de définition associe le réel $u_n$, appelé terme de la suite de rang $n$.

$\mathbb{N}$

##### Compléter : Une suite $u$ est une fonction définie sur $\mathbb{N}$ (ou une partie de $\mathbb{N}$) qui à tout entier $n$ (appelé ???) de son domaine de définition associe le réel $u_n$, appelé terme de la suite de rang $n$.

rang

##### Compléter : Une suite $u$ est une fonction définie sur $\mathbb{N}$ (ou une partie de $\mathbb{N}$) qui à tout entier $n$ (appelé rang) de son domaine de définition associe le réel $u_n$, appelé ???.

terme de la suite de rang $n$

##### À quelles conditions une suite $u$ est-elle croissante (trois réponses possibles) ?

- Pour tout $n$ de son domaine de définition : $u_{n+1}\geq u_{n}$.
- Pour tout $n$ de son domaine de définition : $u_{n+1}-u_{n}\geq0$.
- Si les termes de $u$ sont strictement positifs, pour tout $n$ de son domaine de définition : $\frac{u_{n+1}}{u_{n}}\geq1$.

##### À quelles conditions une suite $u$ est-elle décroissante (trois réponses possibles) ?

- Pour tout $n$ de son domaine de définition : $u_{n+1}\leq u_{n}$.
- Pour tout $n$ de son domaine de définition : $u_{n+1}-u_{n}\leq0$.
- Si les termes de $u$ sont strictement positifs, pour tout $n$ de son domaine de définition : $\frac{u_{n+1}}{u_{n}}\leq1$.

##### À quelles conditions une suite $u$ est-elle constante (trois réponses possibles) ?

- Pour tout $n$ de son domaine de définition : $u_{n}= u_{n+1}$.
- Pour tout $n$ de son domaine de définition : $u_{n+1}-u_{n}=0$.
- La suite $u$ est nulle, ou les termes de $u$ sont non nuls, et pour tout $n$ de son domaine de définition : $\frac{u_{n+1}}{u_{n}}=1$.

#### Subdeck: Suites arithmétiques

##### Quelle est la définition d'une suite arithmétique ?

Une suite $u$ est dite arithmétique s'il existe un réel $r$, appelé *raison*, tel que pour tout $n$ de son domaine de définition, on ait : $u_{n+1}=u_n+r$.

##### À quelles conditions une suite arithmétique est-elle croissante ?

Si sa raison est positive.

##### À quelles conditions une suite arithmétique est-elle strictement croissante ?

Si sa raison est strictement positive.

##### À quelles conditions une suite arithmétique est-elle décroissante ?

Si sa raison est négative.

##### À quelles conditions une suite arithmétique est-elle strictement décroissante ?

Si sa raison est strictement négative.

##### À quelles conditions une suite arithmétique est-elle constante ?

Si sa raison est nulle.

##### Comment prouver qu'une suite est arithmétique ?

On calcule, pour tout $n$ de son ensemble de définition, $u_{n+1}-u_n$ :

- si le résultat est une constante, alors la suite est arithmétique (et cette constante est sa raison) ;
- sinon, elle n'est pas arithmétique.

##### Quel est le terme général (ou formule explicite) d'une suite arithmétique de premier terme $u_0$ et de raison $r$ ?

$u_n=u_0+nr$

##### Quel est le terme général (ou formule explicite) d'une suite arithmétique $u$ de raison $r$, en utilisant le terme $u_p$ ($p$ étant un nombre du domaine de définition de $u$) ?

$u_n=u_p+(n-p)r$

##### Quelle formule permet de calculer la somme de termes consécutifs d'une suite arithmétique ?

$\frac{\left(\text{Premier terme} + \text{Dernier terme}\right)\times\text{Nombre de termes}}{2}$

##### Combien vaut : $1+2+3+4+\cdots+n$ ($n$ étant un nombre entier naturel) ?

$1+2+3+4+\cdots+n=\frac{n(n+1)}{2}$

#### Subdeck: Suites géométriquess

##### Quelle est la définition d'une suite géométrique ?

Une suite $v$ est dite géométrique s'il existe un réel $q$, appelé *raison*, tel que pour tout $n$ de son domaine de définition, on ait : $v_{n+1}=q\times v_n$.

##### À quelles conditions une suite géométrique de premier terme strictement positif est-elle croissante ?

Si sa raison est supérieure à 1.

##### À quelles conditions une suite géométrique de premier terme strictement positif est-elle strictement croissante ?

Si sa raison est strictement supérieure à 1.

##### À quelles conditions une suite géométrique de premier terme strictement positif est-elle décroissante ?

Si sa raison est strictement positive, et inférieure à 1.

##### À quelles conditions une suite géométrique de premier terme strictement positif est-elle strictement décroissante ?

Si sa raison est strictement positive, et strictement inférieure à 1.

##### À quelles conditions une suite géométrique est-elle constante ?

Si sa raison est égale à 1.

##### Comment prouver qu'une suite $u$ est géométrique ?

1. On vérifie si des termes de la suite sont nuls (égaux à 0) :

   - Tous les termes sont égaux à 0 : elle est géométrique.
   - Au moins un terme est égal à 0 et au moins un terme est différent de 0 : elle n'est pas géométrique.
   - Aucun terme n'est égal à 0 : on passe à l'étape 2.

2. On calcule, pour tout $n$ de son ensemble de définition, $\frac{u_{n+1}}{u_n}$ :

   - si le résultat est une constante, alors la suite est géométrique (et cette constante est sa raison) ;
   - sinon, elle n'est pas géométrique.

##### Quel est le terme général (ou formule explicite) d'une suite géométrique $v$ de premier terme $v_0$ et de raison $q$ ?

$v_n=v_0\times q^n$

##### Quel est le terme général (ou formule explicite) d'une suite géométrique $v$ de raison $q$, en utilisant le terme $v_p$ ($p$ étant un nombre du domaine de définition de $v$) ?

$v_n=v_p\times q^{n-p}$

##### Quelle formule permet de calculer la somme de termes consécutifs d'une suite géométrique ?

$\text{Premier terme}\times\frac{1-q^{\text{Nombre de termes}}}{1-q}$

##### Combien vaut : $1+q+q^2+q^3+\cdots+q^n$ (pour $q\neq0$, et $n$ étant un nombre entier naturel) ?

$1+q+q^2+q^3+\cdots+q^n=\frac{1-q^{n+1}}{1-q}$
