# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Dérivation

#### Subdeck: Définition

##### Quelle est la définition du nombre dérivé d'une fonction $f$ en un nombre $a$ ?

Le nombre dérivé de $f$ en $a$, s'il existe, est la limite du taux d'accroissement de la fonction $f$ en $a$ :

$$f'(a)=\lim_{h\rightarrow0}\frac{f(a+h)-f(a)}{h}$$

#### Subdeck: Tangente

##### Quelle est l'interprétation graphique du *nombre dérivé* de la fonction $f$ en $a$ ?

C'est la pente de la tangente à la courbe de $f$ au point d'abscisse $a$.

##### À quoi correspond, pour la fonction $f$, la pente de la tangente à la courbe de $f$ au point d'abscisse $a$.

Le *nombre dérivé* de la fonction $f$ en $a$.

##### Quelle est l'équation de la tangente à la courbe de $f$ au point d'abscisse $a$ (sachant que $f$ est dérivable en $a$) ?

$$y=f'(a)(x-a)+f(a)$$

#### Subdeck: Fonctions usuelles

##### Quel est le domaine de dérivabilité d'une fonction constante ?

$$\mathbb{R}$$

##### Quelle la dérivée d'une fonction constante ?

$$x\mapsto 0$$

##### De quelle fonction $x\mapsto 0$ est-elle la fonction dérivée ?

La fonction constante.

##### Quel est le domaine de dérivabilité d'une fonction affine ?

$$\mathbb{R}$$

##### Quelle la dérivée de la fonction affine $x\mapsto ax+b$ ?

$$x\mapsto a$$

##### De quelle fonction $x\mapsto a$ est-elle la fonction dérivée ($a$ étant un nombre réel) ?

La fonction affine $x\mapsto ax+b$, où $b$ est un nombre réel.

##### Quel est le domaine de dérivabilité de la fonction carré $x\mapsto x^2$ ?

$$\mathbb{R}$$

##### Quelle la dérivée de la fonction carré $x\mapsto x^2$ ?

$$x\mapsto 2x$$

##### De quelle fonction $x\mapsto 2x$ est-elle la fonction dérivée ?

La fonction carré $x\mapsto x^2$.

##### Quel est le domaine de dérivabilité de la fonction cube $x\mapsto x^3$ ?

$$\mathbb{R}$$

##### Quelle la dérivée de la fonction cube $x\mapsto x^3$ ?

$$x\mapsto 3x^2$$

##### De quelle fonction $x\mapsto 3x^2$ est-elle la fonction dérivée ?

La fonction cube $x\mapsto x^3$.

##### Quel est le domaine de dérivabilité de la fonction $x\mapsto x^n$ (où $n$ est un entier naturel non nul) ?

$$\mathbb{R}$$

##### Quelle la dérivée de la fonction $x\mapsto x^n$ (où $n$ est un entier naturel non nul) ?

$$x\mapsto nx^{n-1}$$

##### De quelle fonction $x\mapsto nx^{n-1}$ est-elle la fonction dérivée ($n$ étant un nombre entier naturel non nul) ?

La fonction $x\mapsto x^n$.

##### Quel est le domaine de dérivabilité de la fonction racine carrée $x\mapsto \sqrt{x}$ ?

$\mathbb{R}^{+*}$, c'est-à-dire $\left]0;+\infty\right[$, ou encore tous les nombres réels strictement positifs.

##### Quelle la dérivée de la fonction racine carrée $x\mapsto \sqrt{x}$ ?

$$x\mapsto \frac{1}{2\sqrt{x}}$$

##### De quelle fonction $x\mapsto \frac{1}{2\sqrt{x}}$ est-elle la fonction dérivée ?

La fonction racine carrée $x\mapsto \sqrt{x}$.

##### Quel est le domaine de dérivabilité de la fonction inverse $x\mapsto \frac{1}{x}$ ?

$\mathbb{R}^*$, c'est-à-dire $\left]-\infty;0\right[\cup\left]0;+\infty\right[$, ou encore tous les nombres réels sauf zéro.

##### Quelle la dérivée de la fonction inverse $x\mapsto \frac{1}{x}$ ?

$$x\mapsto -\frac{1}{x^2}$$

##### De quelle fonction $x\mapsto -\frac{1}{x^2}$ est-elle la fonction dérivée ?

La fonction inverse $x\mapsto \frac{1}{x}$.

#### Subdeck: Opérations sur les fonctions

##### Quel est la dérivée de $\lambda u$ (où $\lambda$ est un nombre réel, et $u$ une fonction dérivable) ?

$$\lambda u'$$

##### Quel est la dérivée de $u+v$ (où $u$ et $v$ sont des fonctions dérivables) ?

$$u'+v'$$

##### Quel est la dérivée de $u\times v$ (où $u$ et $v$ sont des fonctions dérivables) ?

$$u'\times v + u\times v'$$

##### Quel est la dérivée de $\frac{u}{v}$ (où $u$ et $v$ sont des fonctions dérivables, et $v$ ne s'annule pas) ?

$$\frac{u'\times v - v'\times u}{v^2}$$

##### Quel est la dérivée de $\frac{1}{v}$ (où $v$ est une fonction dérivable, et ne s'annule pas) ?

$$-\frac{v'}{v^2}$$

##### Quel est la dérivée de $x\mapsto g(ax+b)$ (où $g$ est une fonction dérivable sur l'ensemble des valeurs prises par $x\mapsto ax+b$) ?

$$a\times g'(ax+b)$$

##### À quelles conditions sur la fonction $u$ la fonction $\lambda u$ est-elle dérivable ?

Si la fonction $u$ est dérivable.

##### À quelles conditions sur les fonctions $u$ et $v$ la fonction $u+v$ est-elle dérivable ?

Si les fonctions $u$ et $v$ sont dérivables.

##### À quelles conditions sur les fonctions $u$ et $v$ la fonction $u\times v$ est-elle dérivable ?

Si les fonctions $u$ et $v$ sont dérivables.

##### À quelles conditions sur la fonction $u$ la fonction $\frac{1}{u}$ est-elle dérivable ?

Si :

- la fonction $u$ est dérivable ;
- la fonction $u$ ne s'annule pas.

##### À quelles conditions sur les fonctions $u$ et $v$ la fonction $\frac{u}{v}$ est-elle dérivable ?

Si :

- les fonctions $u$ et $v$ sont dérivables ;
- la fonction $v$ ne s'annule pas.

##### À quelles conditions sur les nombres $a$ et $b$ ou sur la fonction $g$ la fonction $x\mapsto g(ax+b)$ est-elle dérivable ?

Si la fonction $g$ est dérivable sur les valeurs prises par $x\mapsto ax+b$.

#### Subdeck: Variations

##### Si la dérivée $f'$ d'une fonction $f$ est négative sur un intervalle $I$, que peut-on dire de $f$ sur le même intervalle ?

La fonction $f$ est décroissante.

##### Si la dérivée $f'$ d'une fonction $f$ est strictement négative sur un intervalle $I$, sauf éventuellement pour un nombre fini de réels où elle s'annule, que peut-on dire de $f$ sur le même intervalle ?

La fonction $f$ est strictement décroissante.

##### Si la dérivée $f'$ d'une fonction $f$ est positive sur un intervalle $I$, que peut-on dire de $f$ sur le même intervalle ?

La fonction $f$ est croissante.

##### Si la dérivée $f'$ d'une fonction $f$ est strictement positive sur un intervalle $I$, sauf éventuellement pour un nombre fini de réels où elle s'annule, que peut-on dire de $f$ sur le même intervalle ?

La fonction $f$ est strictement croissante.

##### Si la dérivée $f'$ d'une fonction $f$ est nulle sur un intervalle $I$, que peut-on dire de $f$ sur le même intervalle ?

La fonction $f$ est constante.

##### Si une fonction dérivable $f$ est croissante sur un intervalle $I$, que peut-on dire de sa dérivée $f'$ sur le même intervalle ?

La dérivée $f'$ est positive.

##### Si une fonction dérivable $f$ est décroissante sur un intervalle $I$, que peut-on dire de sa dérivée $f'$ sur le même intervalle ?

La dérivée $f'$ est négative.

##### Si une fonction dérivable $f$ est constante sur un intervalle $I$, que peut-on dire de sa dérivée $f'$ sur le même intervalle ?

La dérivée $f'$ est nulle.

##### Si la dérivée $f'$ d'une fonction $f$ s'annule en changeant de signe en $c$, alors la fonction $f$…

… admet un extremum local en $c$.

##### Si une fonction $f$ dérivable sur un intervalle ouvert $I$ admet un extremum local en $c$, alors sa dérivée $f'$…

… s'annule en changeant de signe en $c$.
