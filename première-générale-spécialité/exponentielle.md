# Math

## Subdeck: Première générale : Spécialité

### Subdeck: Fonction exponentielle

#### Subdeck: Définition et propriétés

##### Quelle est la définition de la fonction exponentielle (trois conditions) ?

La fonction exponentielle est l'unique fonction $f$ :

- définie et dérivable sur $\mathbb{R}$ ;
- telle que pour tout nombre réel $x$, on ait : $f'(x)=f(x)$ ;
- et telle que $f(0)=1$.

##### Compléter : $\exp(x+y)=$

$\exp(x+y)=\exp(x)\times\exp(y)$

##### Compléter : $\exp(x)\times\exp(y)=$

$\exp(x)\times\exp(y)=\exp(x+y)$

##### Compléter : $\exp(x-y)=$

$\exp(x-y)=\frac{\exp(x)}{\exp(y)}$

##### Compléter : $\frac{\exp(x)}{\exp(y)}=$

$\frac{\exp(x)}{\exp(y)}=\exp(x-y)$

##### Compléter : $\exp(x)\times\exp(-x)=$

$\exp(x)\times\exp(-x)=1$

##### Compléter : $\exp(-x)=$

$\exp(-x)=\frac{1}{\exp(x)}$

##### Compléter : $\frac{1}{\exp(x)}=$

$\frac{1}{\exp(x)}=\exp(-x)$

##### Compléter (pour $n\in\mathbb{Z}$) : $\exp(nx)=$

$\exp(nx)=\left(\exp(x)\right)^n$

##### Compléter (pour $n\in\mathbb{Z}$) : $\left(\exp(x)\right)^n=$

$\left(\exp(x)\right)^n=\exp(nx)$

#### Subdeck: Nombre e

##### Quelle est la définition de la constante $e$ ?

$e=\exp(1)$

##### Comment exprimer la fonction exponentielle avec le nombre $e$ ?

Pour tout nombre $x\in\mathbb{R}$, on a : $\exp(x)=e^x$.

##### Compléter : $e^{x+y}=$

$e^{x+y}=e^{x}\times e^{y}$

##### Compléter : $e^{x}\times e^{y}=$

$e^{x}\times e^{y}=e^{x+y}$

##### Compléter : $e^{x-y}=$

$e^{x-y}=\frac{e^{x}}{e^{y}}$

##### Compléter : $\frac{e^{x}}{e^{y}}=$

$\frac{e^{x}}{e^{y}}=e^{x-y}$

##### Compléter : $e^{x}\times e^{-x}=$

$e^{x}\times e^{-x}=1$

##### Compléter : $e^{-x}=$

$e^{-x}=\frac{1}{e^{x}}$

##### Compléter : $\frac{1}{e^{x}}=$

$\frac{1}{e^{x}}=e^{-x}$

##### Compléter (pour $n\in\mathbb{Z}$) : $e^{nx}=$

$e^{nx}=\left(e^{x}\right)^n$

##### Compléter (pour $n\in\mathbb{Z}$) : $\left(e^{x}\right)^n=$

$\left(e^{x}\right)^n=e^{nx}$

#### Subdeck: Fonction exponentielle

##### Quelle est la dérivée de la fonction exponentielle ?

La dérivée de la fonction exponentielle est elle-même : $\exp'=\exp$.

##### Dresser le tableau de signes de la fonction exponentielle.

![Exponentielle signe](exponentielle/exponentielle-signes.png)

##### Dresser le tableau de variations de la fonction exponentielle

![Exponentielle variations](exponentielle/exponentielle-variations.png)

##### Tracer la courbe de la fonction exponentielle

![Exponentielle courbe](exponentielle/exponentielle.png)

##### À quelles conditions sur $a$ et $b$ l'équation $e^a=e^b$ est-elle vérifiée ?

Si et seulement si : $a=b$.

##### À quelles conditions sur $a$ et $b$ l'équation $e^a < e^b$ est-elle vérifiée ?

Si et seulement si : $a < b$.

##### À quelles conditions sur $a$ et $b$ l'équation $e^a > e^b$ est-elle vérifiée ?

Si et seulement si : $a > b$.

##### À quelles conditions sur $a$ et $b$ l'équation $e^a \leq e^b$ est-elle vérifiée ?

Si et seulement si : $a \leq b$.

##### À quelles conditions sur $a$ et $b$ l'équation $e^a \geq e^b$ est-elle vérifiée ?

Si et seulement si : $a \geq b$.

##### Quelles sont les solutions de $e^x>0$ ?

Cette inéquation est toujours vraie : tous les nombres réels sont solution.

#### Subdeck: Fonction x↦exp(ax+b)

##### Quelle est la dérivée de la fonction $x\mapsto e^{ax+b}$ (avec $a\neq0$) ?

$x\mapsto ae^{ax+b}$

##### Tracer la fonction $x\mapsto e^{kx}$ (avec $k>0$).

![Exponentielle k positif](exponentielle/exponentielle-k-positif.png)

##### Tracer la fonction $x\mapsto e^{kx}$ (avec $k<0$).

![Exponentielle k négatif](exponentielle/exponentielle-k-negatif.png)

#### Subdeck: Suites

##### Quelle est la nature de la suite définie sur $\mathbb{n}$ par $n\mapsto e^{an}$ (où $a$ est un nombre réel non nul) ?

Cette suite est géométrique de raison $e^a$.
