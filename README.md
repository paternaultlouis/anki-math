# Jeu de cartes Anki pour les mathématiques en lycée

Ce projet a pour but de fournir des paquets de cartes au format [Anki](https://apps.ankiweb.net/) pour le programme de mathématiques des différentes classes de lycée.

Je fais ces cartes au fur et à mesure de mon utilisation, pour les classes que j'enseigne. Il ne sera donc probablement jamais complet.

Visitez [cette page](https://paternaultlouis.forge.apps.education.fr/anki-math) pour télécharger les différents paquets.

---

Louis Paternault 2023-2024 — Publié sous licence [Creative Commons by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

[![Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
