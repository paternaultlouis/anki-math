TEX := $(shell find . -type f -name *.tex)
PNG := $(TEX:.tex=.png)

MD := $(shell find \( -name README.md \) -prune -o -name "*.md" -print)
NIVEAUX := $(patsubst %/,%,$(dir $(wildcard */README.txt)))
APKG := $(MD:.md=.apkg) $(foreach niveau,$(NIVEAUX),public/$(niveau).apkg)

all: $(APKG)

build: all
	python3 build.py

public/%.apkg: $(MD) $(PNG)
	rm -f $@
	mkdir -p public
	md2anki -o-anki $@ -o-md /dev/null -o-md-dir /dev/null $$(find $* -type f -name "*.md")

#.SECONDEXPANSION:
%.apkg: %.md $(PNG) # $$(patsubst .tex,.png,$$(wildcard %/*.tex))
	rm -f $@
	mkdir -p public/$(@D)
	md2anki -o-anki public/$@ -o-md /dev/null $<

.SECONDARY:
.ONESHELL:
%.png: %.pdf
	cd $(@D)
	pdftoppm -singlefile -png $(<F) $(*F)

.ONESHELL:
%.pdf: %.tex
	cd $(@D)
	lualatex $(<F)
