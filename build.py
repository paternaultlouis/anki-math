#!/usr/bin/env python

# Copyright 2023-2024 Louis Paternault
#
# Released under the following licenses:
# - [Do What The Fuck You Want To Public License](http://www.wtfpl.net/)
#   or its French translation [Licence Publique Rien À Branler](ttp://sam.zoy.org/lprab/) ;
# - [CC0 1.0 universel](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) ;
# - GNU GPL3 or any later version;
# - public domain, if applicable in your country.

"""Génère une page HTML présentant les différents paquets de carte du dépôt."""

import logging
import pathlib

import jinja2

logging.basicConfig(level=logging.INFO)

ROOT = pathlib.Path(__file__).parent
PUBLIC = ROOT / "public"

TEMPLATE = """
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Jeux de cartes Anki pour les math en lycée</title>
    <link rel="icon" type="image/x-icon" href="favicon.ico">
  </head>
  <body>
  <h1>⭐ Jeux de cartes Anki pour les mathématiques en lycée</h1>

  <p>
  Ces jeux de carte sont au format <a href="https://docs.ankiweb.net/exporting.html#deck-apkg">apkg</a>. Ils peuvent être ouverts avec les applications officielles <a href="https://apps.ankiweb.net/">Anki</a>.
  </p>

  <p>
  Ces jeux de cartes sont aussi disponibles <a href="https://ankiweb.net/shared/by-author/940066280">sur le AnkiWeb</a>.
  </p>

  <h2>Jeux de carte</h2>

  <ul>
    {% for node in tree|sort %}
    <li> {{tree[node].nom}} : <a href="{{node}}.apkg">l'année complète</a> ou par chapitre :
        <ul>
            {% for sub in tree[node].sub|sort %}
                <li><a href="{{sub}}">{{sub.name}}</a></li>
            {% endfor %}
        </ul>
    </li>
    {% endfor %}
    </ul>

  <h2>Plus d'informations</h2>
  <ul>
      <li>La <a href="https://ncase.me/remember/fr.html">répétition espacée</a>, mise en œuvre par le logiciel <a href="https://apps.ankiweb.net/">Anki</a>.</li>
      <li>Ces jeux de cartes sont générés à partir de fichier markdown avec l'outil <a href="https://github.com/AnonymerNiklasistanonym/Md2Anki">Md2Anki</a>.</li>
      <li>Les <a href="https://forge.apps.education.fr/paternaultlouis/anki-math">sources</a> de ce projet.</li>
  </ul>


    <footer>
    <h2>À propos</h2>
    Copyright 2023—2024 Louis Paternault — Publié sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Creative Commons by-nc-sa 4.0</a>
    <br>
    Une remarque ? Une question ? Une erreur ? <a href="https://ababsurdo.fr/apropos/">Contactez moi…</a>
    </footer>
  </body>
</html>
""".strip()


def readme(path):
    """Renvoit le contenu du README dans le chemin donné en argument."""
    with open(path / "README.txt", mode="r", encoding="utf8") as content:
        return content.read().strip()


def gather(path):
    """Cherche et renvoit les fichiers apkg

    Ces fichiers sont rassemblés en arbre représentant les dossiers et sous-dossiers
    """
    tree = {}
    for sub in path.iterdir():
        if sub.match("*.apkg"):
            logging.info("""Nouveau niveau trouvé : %s""", sub.stem)
            tree[sub.stem] = {
                "nom": readme(ROOT / sub.stem),
                "sub": list(
                    file.relative_to(PUBLIC)
                    for file in (path / sub.stem).iterdir()
                    if file.suffix == ".apkg"
                ),
            }
    return tree


def generate(sourcedir, destfile):
    """Génère le fichier HTML"""
    with open(destfile, mode="w", encoding="utf8") as dest:
        logging.info("""Génération dans le dossier "%s"…""", dest.name)
        dest.write(
            jinja2.Environment().from_string(TEMPLATE).render(tree=gather(sourcedir))
        )


if __name__ == "__main__":
    generate(
        PUBLIC,
        PUBLIC / "index.html",
    )
